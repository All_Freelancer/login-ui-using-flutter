import 'package:flutter/material.dart';
import 'clipper.dart';                  //CLASS CLIPPER
import 'package:login_app/src/ui/widgets/all_wigets.dart';
import 'package:login_app/src/ui/widgets/login-register.dart';


//****************************** CLASS UI LOGIN ********************************
class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

//******************************* CREATE LAYOUT ********************************
class _HomeState extends State<Home> {

  AllWidgets _allWidgets;
  LoginRegister _loginRegister;

  //:::::::::: VARIABLE GLOBAL
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  //::::::::::: VARIABLE CONTROLLER
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _nameController = new TextEditingController();

  //::::::::::: VARIABLE
  String _email;
  String _password;
  String _displayName;
  bool _obsecure = false;

  Color primary;

  //************************** METHOD STATE ************************************
  void initState() {
    super.initState();

    _allWidgets = AllWidgets();
    _loginRegister = LoginRegister();
  }

  //************************* METHOD - BUILD WIDGET ****************************
  @override
  Widget build(BuildContext context) {
    //::::::::::::::::::::::::
    //Color primary = Theme.of(context).primaryColor;
    primary = Theme.of(context).primaryColor;

    //:::::::::::::::::::::::::::: WIDGET LAYOUT :::::::::::::::::::::::::::
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        key: _scaffoldKey,                  // FOR ACCESS THIS WIDGET (INCLUDE)
        backgroundColor: Theme.of(context).primaryColor,

        //::::::::::::::::::: BODY --- COLUMN ::::::::::::::::::::
        body: Column(
          children: <Widget>[
            //:::::::::::::::::::::::::::: LOGO CREATE :::::::::::::::::::::::::
            _allWidgets.logo(context),

            //::::::::::::::::::::::::::: BUTTON LOGIN :::::::::::::::::::::::::
            Padding(
              child: Container(
                child: _allWidgets.button("LOGIN", primary, Colors.white, Colors.white,
                    primary, _loginSheet),
                height: 50,
              ),
              padding: EdgeInsets.only(top: 80, left: 20, right: 20),
            ),

            //:::::::::::::::::::::::::: BUTTON REGISTER :::::::::::::::::::::::
            Padding(
              padding: EdgeInsets.only(top: 10, left: 20, right: 20),
              
              child: Container(
                //:::::::::::::::::: HEIGHT CONTAINER
                height: 50,

                //:::::::::::::::: BUTTON :::::::::::
                child: OutlineButton(        // DESIGNER BUTTON IN CONTAINER
                  highlightedBorderColor: Colors.white,
                  borderSide: BorderSide(color: Colors.white, width: 2.0),
                  highlightElevation: 0.0,
                  splashColor: Colors.white,
                  highlightColor: Theme.of(context).primaryColor, //COLOR CLICK
                  color: Theme.of(context).primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0),
                  ),

                  //:::::::::::::: TEXT IN CONTAINER
                  child: Text(
                    "REGISTER",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 20),
                  ),

                  //:::::::::::::: CLICK
                  onPressed: () {
                    _registerSheet();  // CALL FUNCTION (METHOD)
                  },
                ),
              ),
            ),

            //:::::::::::::::::::::::: EXPANDED NEW WIDGET :::::::::::::::::::::
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,

                child: ClipPath(
                  child: Container(
                    color: Colors.white,
                    height: 300,
                  ),
                  clipper: BottomWaveClipper(),  // CALL CLASS IN CLIPPER
                ),

              ),
            )

          ],

          crossAxisAlignment: CrossAxisAlignment.stretch,
        ));
  }


  /// ********************* LOGIN AND REGISTER FUNCTIONS ***********************
  void _loginUser() {
    _email = _emailController.text;
    _password = _passwordController.text;
    _emailController.clear();
    _passwordController.clear();
  }

  void _registerUser() {
    _email = _emailController.text;
    _password = _passwordController.text;
    _displayName = _nameController.text;
    _emailController.clear();
    _passwordController.clear();
    _nameController.clear();
  }

  //******************************** LOGIN SHEET *******************************
  void _loginSheet() {
    //:::::::::::: ACCESS IN LAYOUT PRIMARY AND EXPANDED NEW WIDGET ::::::::::
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return DecoratedBox(
        decoration: BoxDecoration(color: Theme.of(context).canvasColor),

        child: ClipRRect(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0)),
          child: Container(

            //:::::::::::::::::::::::: LIST VIEW ::::::::::::::::::::
            child: ListView(
              children: <Widget>[
                //******************** CLOSE CONTAINER (LAYOUT):::::::::::::
                _loginRegister.close(context, true, resetTextField),

                //*********************** SCROLL WIDGET IN LIST VIEW **********
                SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      //------------------- LOGO DESIGNER LOGIN -------------
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 140,
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Align(
                                child: Container(
                                  width: 130,
                                  height: 130,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Theme.of(context).primaryColor),
                                ),
                                alignment: Alignment.center,
                              ),
                            ),
                            Positioned(
                              child: Container(
                                child: Text(
                                  "LOGIN",
                                  style: TextStyle(
                                    fontSize: 48,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                                alignment: Alignment.center,
                              ),
                            ),
                          ],
                        ),
                      ),

                      //-------------------- TEXT FIELD ---------------------
                      Padding(
                        padding: EdgeInsets.only(bottom: 20, top: 60),
                        child: _allWidgets.input(context, Icon(Icons.email), "EMAIL",
                            _emailController, false),
                      ),

                      //---------------------- TEXT FIELD -------------------
                      Padding(
                        padding: EdgeInsets.only(bottom: 20),
                        child: _allWidgets.input(context, Icon(Icons.lock), "PASSWORD",
                            _passwordController, true),
                      ),

                      //--------------------- SPACE
                      SizedBox(
                        height: 20,
                      ),

                      //----------------------- BUTTON FOR LOGIN
                      Padding(
                        padding: EdgeInsets.only(
                            left: 20,
                            right: 20,
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        child: Container(
                          //:::::::: CALL METHOD CREATE BUTTON
                          child: _allWidgets.button("LOGIN", Colors.white, primary,
                              primary, Colors.white, _loginUser),

                          height: 50,
                          width: MediaQuery.of(context).size.width,
                        ),
                      ),

                      //----------------------- SPACE
                      SizedBox(
                        height: 20,
                      ),

                    ],
                  ),
                ),
              ],
            ),

            //:::::::::::::::::::::: ATTRIBUTE CONTAINER ::::::::::::
            height: MediaQuery.of(context).size.height / 1.1,
            width: MediaQuery.of(context).size.width,
            color: Colors.white,

          ),
        ),
      );
    });
  }

  //******************************** REGISTER SHEET ****************************
  void _registerSheet() {
    //:::::::::::: ACCESS IN LAYOUT PRIMARY AND EXPANDED NEW WIDGET ::::::::::
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return DecoratedBox(
        decoration: BoxDecoration(color: Theme.of(context).canvasColor),

        child: ClipRRect(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0)),

          child: Container(

            //:::::::::::::::::::::::: LIST VIEW ::::::::::::::::::::
            child: ListView(
              children: <Widget>[
                //******************** CLOSE CONTAINER (LAYOUT):::::::::::::
                _loginRegister.close(context, false, resetTextField),

                //*********************** SCROLL WIDGET IN LIST VIEW **********
                SingleChildScrollView(
                  child: Column(children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 140,
                      child: Stack(
                        children: <Widget>[
                          Positioned(
                            child: Align(
                              child: Container(
                                width: 130,
                                height: 130,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Theme.of(context).primaryColor),
                              ),
                              alignment: Alignment.center,
                            ),
                          ),
                          Positioned(
                            child: Container(
                              padding: EdgeInsets.only(bottom: 25, right: 40),
                              child: Text(
                                "REGI",
                                style: TextStyle(
                                  fontSize: 44,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                              alignment: Alignment.center,
                            ),
                          ),
                          Positioned(
                            child: Align(
                              child: Container(
                                padding: EdgeInsets.only(top: 40, left: 28),
                                width: 130,
                                child: Text(
                                  "STER",
                                  style: TextStyle(
                                    fontSize: 40,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              alignment: Alignment.center,
                            ),
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.only(
                        bottom: 20,
                        top: 60,
                      ),
                      child: _allWidgets.input(context, Icon(Icons.account_circle), "DISPLAY NAME",
                          _nameController, false),
                    ),

                    Padding(
                      padding: EdgeInsets.only(
                        bottom: 20,
                      ),
                      child: _allWidgets.input(context,
                          Icon(Icons.email), "EMAIL", _emailController, false),
                    ),

                    Padding(
                      padding: EdgeInsets.only(bottom: 20),
                      child: _allWidgets.input(context, Icon(Icons.lock), "PASSWORD",
                          _passwordController, true),
                    ),

                    Padding(
                      padding: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          bottom: MediaQuery.of(context).viewInsets.bottom),
                      child: Container(
                        child: _allWidgets.button("REGISTER", Colors.white, primary,
                            primary, Colors.white, _registerUser),
                        height: 50,
                        width: MediaQuery.of(context).size.width,
                      ),
                    ),

                    SizedBox(
                      height: 20,
                    ),
                  ]),
                ),
              ],
            ),

            //:::::::::::::::::::::: ATTRIBUTE CONTAINER ::::::::::::
            height: MediaQuery.of(context).size.height / 1.1,
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            
          ),
        ),
      );
    });
  }

  //***************************** RESET TEXT FIELD *****************************
  void resetTextField (bool loginRegister){

    if(loginRegister){
      _emailController.clear();
      _passwordController.clear();

    } else{
      _emailController.clear();
      _passwordController.clear();
      _nameController.clear();
    }
    Navigator.of(context).pop();
  }

  //****************************************************************************
}
