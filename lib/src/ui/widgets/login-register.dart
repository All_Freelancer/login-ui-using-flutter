
import 'package:flutter/material.dart';

class LoginRegister{

  //*****************************************
  Widget filledButton(String text, Color splashColor, Color highlightColor,
      Color fillColor, Color textColor, bool clear, function ) {

    return RaisedButton(
      highlightElevation: 0.0,
      splashColor: splashColor,
      highlightColor: highlightColor,
      elevation: 0.0,
      color: fillColor,
      shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0)),
      child: Text(
        text,
        style: TextStyle(
            fontWeight: FontWeight.bold, color: textColor, fontSize: 20),
      ),
      onPressed: () {
        function(clear);
      },
    );
  }

  //************************ CLOSE CONTAINER (LAYOUT) **************************
  Widget close(BuildContext context, bool clear, Function resetTextField){

    return Container(
      child: Stack(
        children: <Widget>[
          Positioned(
            //left: 10,
            right: 10,
            top: 10,
            child: IconButton(
              onPressed: () {
                resetTextField(clear); // CALL FUNCTION (METHOD)
              },
              icon: Icon(
                Icons.close,
                size: 30.0,
                color: Theme.of(context).primaryColor,
              ),
            ),
          )
        ],
      ),
      height: 50,
      width: 50,
    );
  }

  //****************************************************************************
}
