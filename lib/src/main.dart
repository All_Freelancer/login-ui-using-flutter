import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'ui/launch/home.dart';


//****************************** RUN APP ***************************************
//void main() => runApp(MyApp());

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MyApp());
  });
}

//***************************** CLASS APP **************************************
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',                  // TITLE APP
      debugShowCheckedModeBanner: false,
      //:::::::::::::::::: DEFAULT ALL APP :::::::::::
      theme: ThemeData(
          hintColor: Color(0xFFC0F0E8),
          primaryColor: Color(0xFF80E1D1),
          fontFamily: "Montserrat",
          canvasColor: Colors.transparent     // FOR CONNOR NEW WIDGET
      ),
      home: Home(),                  // INIT FIRST LAYOUT - WIDGET CLASS
    );
  }
}
//******************************************************************************
